package ar.edu.utnfc.argprog;

//import ar.edu.utnfc.argprog.streams.numeros.Ejemplo;
import ar.edu.utnfc.argprog.streams.personas.Ejemplo;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException
    {
        Ejemplo.start();

    }
}
