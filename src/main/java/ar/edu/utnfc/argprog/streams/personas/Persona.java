package ar.edu.utnfc.argprog.streams.personas;

public class Persona {

    private int documento;
    private String nombre;
    private String apellido;
    private int edad;

    public int getDocumento() {
        return documento;
    }

    public void setDocumento(int documento) {
        this.documento = documento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Persona() {

    }

    public Persona(int documento, String nombre, String apellido, int edad) {
        this.documento = documento;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }

    public Persona(String linea) {
        String[]valores = linea.split(";");
        this.documento = Integer.valueOf(valores[0]);
        this.nombre = valores[1];
        this.apellido = valores[2];
        this.edad = Integer.valueOf(valores[3]);
    }

    public String nombreCompleto() {
        return nombre + " " + apellido;
    }

    public String toString() {
        return documento + " " + nombre + " " + apellido + " " + edad;
    }
}
