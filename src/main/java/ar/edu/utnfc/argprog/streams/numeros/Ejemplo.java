package ar.edu.utnfc.argprog.streams.numeros;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Ejemplo {
    public static void start() throws IOException {

        // Stream de números literales
//        Stream<Integer> numeros = Stream.of(7, 5, 29, 11, 22, 6, 9, 3);
        List<Integer> numeros = Stream.of(7, 5, 29, 11, 22, 6, 9, 3)
                .toList();

//        numeros.forEach(System.out::println);
        numeros.stream()
//                .map((a) -> a * 3)
                .map(new Function<Integer, Integer>() {
                    @Override
                    public Integer apply(Integer a) {
                        return a * 3;
                    }
                })
//                .filter((a) -> a % 2 != 0)
                .filter(new Predicate<Integer>() {
                    @Override
                    public boolean test(Integer a) {
                        return a % 2 != 0;
                    }
                })
//                .forEach(System.out::println);
//                .forEach((a) -> System.out.println(a))
                .forEach(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer a) {
                        System.out.println(a);
                    }
                });


//        int Suma = numeros.reduce(0, (a, b) -> a + b);
        int suma = numeros.stream().reduce(0, (a, b) -> a + b);
        System.out.println("Suma: " + suma);


        // Ejemplo suministrado con el material adicional.
        // Revisión
//        List<Integer> numeros = Files.lines(Paths.get("numeros.txt"))
//                .map(Integer::valueOf)
//                .toList();
//
//        // Listado de todos los numeros
//        System.out.println("Listado de todos los numeros:");
//        numeros.stream().forEach(System.out::println);
//
//        // Cantidad de pares
//        long cantidad = numeros.stream()
//                .filter(x -> x % 2 == 0)
//                .count();
//        System.out.println("Hay " + cantidad + " pares");
//
//        // Raices cuadradas de los 20 menores
//        numeros.stream()
//                .sorted()
//                .limit(20)
//                .map(Math::sqrt)
//                .forEach(System.out::println);

    }
}
